let section = document.querySelector(".sectionMessages")

function enviarMensagem() {
    let input = document.querySelector(".inputText")
    let message = input.value
    console.log(message)
    let escopo = document.createElement("div")
    let texto = document.createElement("p")
    texto.innerText = input.value
    escopo.append(texto)
    let section = document.querySelector(".sectionMessages")
    section.append(escopo)
}

function limparMensagem(){
    section.innerText = ""
}

let button_send = document.querySelector("#button_send")
button_send.addEventListener("click",()=>enviarMensagem())

let button_clean = document.querySelector("#button_clean")
button_clean.addEventListener("click", limparMensagem)
